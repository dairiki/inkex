# Python Wheels for `inkex`

This is a fork of the
[Inkscape/extensions](https://gitlab.com/inkscape/extensions)
repository, edited just enough to be able to build Python wheels of
the `inkex` package corresponding to version of `inkex` included in
each release of [Inkscape](https://inkscape.org/).  These wheels are
currently published in this project’s Python [package
registry](https://gitlab.com/dairiki/inkex/-/packages), from which
they may be installed using [`pip`](https://pip.pypa.io/en/stable/) or
another similar Python package management tool.

The primary use case for these packages is for use in unit testing and
CI testing of custom Python-based Inkscape extension code.  For that
case, it offers a couple of advantages, including:

- The ability to install `inkex` without bothering with a full
  installation of Inkscape.
- The ability to easily test with versions of `inkex` corresponding to
  different versions of Inkscape.

## Version Numbers

Here we use the version number of the corresponding release of
Inkscape, with a [local version
identifier](https://peps.python.org/pep-0440/#local-version-identifiers)
appended.  For example, the first release here for the Inkscape 1.2.1
version of the `inkex` code is `1.2.1+dairiki.1`.  Should additional
releases be necessary, they will be numbered `1.2.1+dairiki.2`, and so
on.



## Usage

The python simple package index for this project’s package registry is
at <https://gitlab.com/api/v4/projects/40060814/packages/pypi/simple>.

To install `inkex` corresponding to the latest release of Inkscape,
try something like:

```sh
export PIP_EXTRA_INDEX_URL=https://gitlab.com/api/v4/projects/40060814/packages/pypi/simple

pip install inkex

```

(As an alternative to setting `$PIP_EXTRA_INDEX_URL` one can use the
`pip`’s
[`--extra-index-url`](https://pip.pypa.io/en/stable/cli/pip_install/#cmdoption-extra-index-url)
parameter.  The `--extra-index-url` parameter can also be included in
`requirement.txt` files.)


To install `inkex` corresponding to a specific release (say 1.1.2) of
Inkscape try:

```sh
export PIP_EXTRA_INDEX_URL=https://gitlab.com/api/v4/projects/40060814/packages/pypi/simple

pip install inkex==1.1.2.*
```

(The `.*` is necessary to allow for matching the `+dairiki.<n>` local
version specifier.)

### Install “Extras”

The dependency on `PyGObject`, required by the `inkex.gui` package
(which is new in Inkscape 1.2) has been made optional.  (Installing
`PyGObject` is a bit of a heavy lift.  It is available from PyPI only
as an _sdist_, and installation requires that a [number of shared
libraries and there development headers][pygobject-installation] be
installed.)

[pygobject-installation]: https://pygobject.readthedocs.io/en/latest/getting_started.html

If you need it, install the `gui` extra:

```sh
pip install inkex[gui]
```

## Example Tox Usage

Here’s a minimal `tox.ini` to test a project under a matrix of Python × Inkscape versions.

```ini
[tox]
envlist = py{37,38,39,310}-inkex{10,11,12}

[testenv]
commands = pytest {posargs:tests}
deps =
    pytest

    # Install inkex packages from python registry at https://gitlab.com/dairiki/inkex/
    --extra-index-url=https://gitlab.com/api/v4/projects/40060814/packages/pypi/simple
    inkex
    inkex10: inkex==1.0.*
    inkex11: inkex==1.1.*
```

## Additions to upstream

A bit of code has been added to the upstream version of `inkex`.
Currently this added code all has to do with providing help for
testing `.inx` files.

### INX file schema checks

A new method, `.assertInxSchemaValid`, has been added to the
`inkex.tester.inx.InxMixin` class.  This method may be used to test an
`.inx` file against the XML schema in the upstream repo.

For example, you may write a test for your `.inx` file like

```python
class TestMyInxFile(InxMixin, unittest.TestCase):
    def test(self):
        inx_file = "path/to/my.inx"

        # This assertion is unchanged from upstream.
        # It checks that the parameters declared in the .inx
        # file match that of the python extension command.
        self.assertInxIsGood(inx_file)

        # This runs xmllint to check the .inx file against whatever
        # XML schema files are provided by upstream.
        self.assertInxSchemaValid(inx_file)
```

### INX file test script

There is also a new executable module which can be used from the
command line to run the `assertInxIsGood` and `assertInxSchemaValid`
checks.

Run it like:

```sh
python -m inkex.tester.test_inx_file *.inx
```
